#!/usr/bin/env ruby
require 'discordrb'
require 'youtube-dl'
require 'fileutils'
bot = Discordrb::Commands::CommandBot.new token: 'NDAxMTg3MDU5Nzg5NzI1NzA2.DTqNCw.rCYmQX8UuWgDO5ylCIy1VCAoB88', prefix: '!'

#folder size is the size of the music folder in bytes
#playlist is the songs stored in ~/bmusic/
folder_size = 0
playlist = []
music_dir = "#{Dir.home}/bmusic/"
puts music_dir
Dir.mkdir music_dir unless File.directory?(music_dir) 
Dir["#{music_dir}*"].each do |x|
	folder_size+=File.size(x)
	playlist << File.basename(x)
end

#playlist_count is the index of the current song that is playing
playlist_count = nil
volume = nil

#voice
##Voice starts by setting variables
##encryption is turned off because it takes 3x processing to use it
##no encryption can create a dumb error about vws that cannot be googled ###I need to find a fix for this switch encryption=false to true
bot.command :start do |event|
	#returns "fuck off" if already playing
	next "Fuck off" if playlist_count
	channel = event.user.voice_channel
	#returns get in voice if user not in voice
	next "Get in voice" unless channel
	Thread.abort_on_exception = false
	bot.voice_connect(channel, encryption=true)
	voice_bot = event.voice
	voice_bot.encoder.bitrate = 64000 
	voice_bot.volume = volume if volume
	playlist_count = 0
	song_loop = nil
	last_message = event.respond("playing music in "+channel.name)
	##The actual voice is in the thread voice_thread and the bot waits for commands that start with !
	##playlist_count determines what song in the playlist is playing
	##runtime exception is raised when the song is skipped
	voice_thread = Thread.new {
		while 1 
			begin
				playlist_count = 0 if playlist_count>=playlist.length
				sleep(0.1) while playlist.length == 0
				last_message.delete 
				last_message = event.respond("playing "+format(playlist[playlist_count]))
				voice_bot.play_file("#{music_dir}"+playlist[playlist_count])
				playlist_count+=1 unless song_loop
			rescue
			end
		end
	}
	##bot waits for commands from users
	##if false is returned then it continues to wait for commands
	##case is used to see what command the user used
	event.channel.await("!") do |play_event|
		command, arg = play_event.message.content.split(" ")
		command = nil unless play_event.user.voice_channel == channel
		case command 
		when "!skip"
			playlist_count = arg.to_i if arg
			playlist_count+=1 unless arg
			voice_bot.stop_playing
			voice_thread.raise
			voice_bot.continue
			false
		when "!end"
			voice_bot.stop_playing
			voice_thread.kill
			playlist_count = nil
			sleep(0.1) while voice_thread.alive?
			voice_bot.destroy
			play_event.respond("goodbye")
			true
		when "!test"
			play_event.respond(voice_thread.alive?)
			false
		when "!volume"
			begin
				volume = Float(arg)
				break if volume>200
				volume/=100.0
				voice_bot.volume = volume
			rescue ArgumentError, TypeError
				#snarky comment
			end
			false
		when "!pause"
			voice_bot.pause
			false
		when "!play"
			voice_bot.continue
			false
		when "!loop"
			if song_loop
				event.respond("stopped looping current song")
				song_loop = nil
			else
				event.respond("looping current song")
				song_loop = true
			end
			false
		else
			false
		end
	end
end

bot.command :add do |event, *args|
	link=""
	args.each do |x|
		next if link.include? x or x == "/"
		event.channel.start_typing
		if folder_size>109961627776
			event.respond("too many songs in playlist")
			return
		end
		if !x.include? "https://" and !x.include? "http://"
			link="ytsearch1:"
			args[args.index(x), args.length].each do |y|
				if y.include? "https://" or y.include? "http://" or y == "/"
					download(link)
					break
				else
					link+=y+" "
					download(link) if y == args[-1]
				end
			end
		elsif x == args[-1]
			Thread.new { download(x) }.join
		else
			Thread.new { download(x) }
		end
	end
	Dir["#{music_dir}/*"].each do |x|
		unless playlist.include? x
			folder_size+=File.size(x)
			playlist << File.basename(x)
		end
	end
	event.respond("finished downloading")
end

bot.command :addplaylist do |event, arg|
	if folder_size>109961627776
		event.respond("too many songs in playlist")
		return
	end
	event.respond("downloading playlist")
	downloadpl(arg)
	Dir["#{music_dir}/*"].each do |x|
		unless playlist.include? x
			folder_size+=File.size(x)
			playlist << File.basename(x)
		end
	end
	event.respond("finished downloading playlist")
end

bot.command :delete do |event, *args|
	next "nothing to delete" if playlist.length == 0
	files_to_delete = []
	args.each do |x|
		begin
			x = Integer(x)
			raise ArgumentError if x>playlist.length
			file = playlist[x.to_i]
			files_to_delete << file
			folder_size-=File.size("#{music_dir}"+file)
			File.delete("#{music_dir}"+file)
			event.respond("removed "+format(file))
		rescue ArgumentError, TypeError
			event.respond(x+" is not valid")
		end
	end
	playlist -= files_to_delete
	event.respond("deleted files")
end

bot.command :playlist do |event|
	next "nothing in playlist" if playlist.length == 0
	outputs = []
	output="```"
	count = 0
	playlist.each do |x|
		x = format(x)
		if output.length > 1800
			output+="```"
			outputs << output
			output="```"
		end
		if count == playlist_count
			output+=count.to_s+" "+x+"  <= currently playing\n"
		else
			output+=count.to_s+" "+x+"\n"
		end
		count+=1
	end
	output+="```"
	if outputs.any?
		outputs.each do |eachoutput| 
			event.respond(eachoutput)
		end
	end
	event.respond(output)
end

bot.command :np do |event|
	next "nothing playing" unless playlist_count
	event.respond(format(playlist[playlist_count]))
end

bot.command :clear do |event|
	FileUtils.rm_r music_dir
	playlist = []
	folder_size = 0
	Dir.mkdir music_dir
	event << "deleted everything"
end

bot.command :shuffle do |event|
	new_playlist = playlist.shuffle
	playlist_count = new_playlist.index playlist[playlist_count] unless playlist_count == nil
	playlist = new_playlist
	event << "Shuffled btw"
end

bot.command :help do |event|
	event << "```
	Playlist commands:
		!playlist shows the playlist
		!add (url) adds song from website see: \"rg3.github.io/youtube-dl/supportedsites.html\" for supported sites
		!add (string) does a youtube search for string and the downloads first video it finds
		!addplaylist adds songs from a youtube playlist
		!delete (int) deletes a song from the playlist
		!clear deletes all songs from playlist
		!shuffle shuffles the playlist```"
	event << "```
	Voice commands:
		!start to play songs
		!end to disconnect from voice
		!skip {int}*optional* skips to the next song in the playlist or to given argument
		!volume (int) changes volume from 0-200 default: 100
		!pause pauses song
		!play plays song
		!loop loops the current song```"
end


def download(link)
	download_dir = "#{Dir.home}/botmusic/"
	options = {
		no_playlist: true,
		extract_audio: true,
		audio_format: 'best',
		output: "#{download_dir}%(title)s.%(ext)s",
		max_filesize: '250m',
		audio_quality: '64k',
	}
	YoutubeDL.download link, options
end

def downloadpl(link)
	command = "youtube-dl -j --flat-playlist "+link+" |jq -r \'.id\' | sed \'s_^_https://youtube.com/v/_\'"
	links = %x[ #{command} ]
	linkslist = links.split("\n")
	linkslist.each do |x|
		if x == linkslist[-1]
			Thread.new { download(x) }.join
		else
			Thread.new { download(x) }
		end
	end
end

def format(stringbtw)
	stringbtw = File.basename(stringbtw, ".*")
	return stringbtw
end

bot.run
