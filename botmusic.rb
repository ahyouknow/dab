#!/usr/bin/env ruby
require 'discordrb'
require 'youtube-dl'
require 'fileutils'
bot = Discordrb::Commands::CommandBot.new token: 'NDAxMTg3MDU5Nzg5NzI1NzA2.DTqNCw.rCYmQX8UuWgDO5ylCIy1VCAoB88', prefix: '!'

#folder size is the size of the music folder in bytes
#playlist is the songs stored in ~/bmusic/
music_dir = "#{Dir.home}/bmusic/"
Dir.mkdir music_dir unless File.directory?(music_dir) 

#playlist_count is the index of the current song that is playing
playlist_count = nil
volume = nil
voice_bot = nil
song_loop = nil
playlist = []
folder_size = 0
voice_thread = Thread.new { }

#Voice
bot.command :start do |event|
	#returns "fuck off" if already playing
	next "Fuck off" if playlist_count
	channel = event.user.voice_channel
	#returns get in voice if user not in voice
	next "Get in voice" unless channel
	last_message = event.respond("playing music in "+channel.name)
	Thread.abort_on_exception = false
	bot.voice_connect(channel, encryption=true)
	voice_bot = event.voice
	voice_bot.encoder.bitrate = 96000 
	voice_bot.volume = volume if volume
	playlist_count = 0
	song_loop = nil
	##playlist_count determines what song in the playlist is playing
	##runtime exception is raised when the song is skipped
	voice_thread = Thread.new {
		while 1 
			begin
				playlist_count = 0 if playlist_count>=playlist.length
				sleep(0.1) while playlist.length == 0
				last_message.delete 
				last_message = event.respond("playing "+format(playlist[playlist_count]))
				voice_bot.play_file("#{music_dir}"+playlist[playlist_count])
				playlist_count+=1 unless song_loop
			rescue
			end
		end
	}
	next
end

bot.command :skip do |event, arg|
	next "!start to start voice" unless voice_thread.alive?
	playlist_count = arg.to_i if arg
	playlist_count+=1 unless arg
	voice_bot.stop_playing
	voice_thread.raise
	voice_bot.continue
	next
end

bot.command :end do |event|
	next "!start to start voice" unless voice_thread.alive?
	voice_bot.stop_playing
	voice_thread.kill
 	playlist_count = nil
	sleep(0.1) while voice_thread.alive?
	voice_bot.destroy
	event << "goodbye"
end

bot.command :test do |event|
	event.respond(voice_thread.alive?)
end

bot.command :volume do |event, arg|
	next "!start to start voice" unless voice_thread.alive?
	begin
		volume = Float(arg)
		next if volume>200
		volume/=100.0
		voice_bot.volume = volume
	rescue ArgumentError, TypeError
		#snarky comment
	end
	event << "Volume is #{arg}"
end

bot.command :pause do |event|
	next "!start to start voice" unless voice_thread.alive?
	voice_bot.pause
	event << "paused"
end

bot.command :play do |event|
	next "!start to start voice" unless voice_thread.alive?
	voice_bot.continue
	event << "playing"
end

bot.command :loop do |event|
	next "!start to start voice" unless voice_thread.alive?
	if song_loop
		song_loop = nil
		event << "stopped looping current song"
	else
		song_loop = true
		event << "looping current song"
	end
end

bot.command :update do |event|
	playlist, folder_size = updatepl([], 0)
	event << "updated the playlist"
end
	
bot.command :add do |event, *args|
	for arg in args
		if folder_size>109961627776
			event.respond("too many songs in playlist")
			lastthread.join
			break
		end
		unless arg.include? "https://" or arg.include? "http://"	
			event.respond "#{arg} is not a link. use !addsearch"
			next
		end
		lastthread = Thread.new { download(arg)	}
	end
	event.channel.start_typing
	lastthread.join
	playlist, folder_size = updatepl(playlist, folder_size)
	event << "finished downloading"
end

bot.command :addsearch do |event, arg|
	next "nonlinks only. use !add" if arg.include? "https://" or arg.include? "http://"
	next "too many songs in playlist" if folder_size>109961627776
	search = "ytsearch1: #{arg}"
	event.channel.start_typing
	download(search)
	playlist, folder_size = updatepl(playlist, folder_size)
	event << "finished downloading"
end

bot.command :addplaylist do |event, arg|
	next "too many songs in playlist" if folder_size>109961627776
	event.respond("downloading playlist")
	downloadpl(arg)
	playlist, folder_size = updatepl(playlist, folder_size)
	event << "finished downloading playlist"
end

bot.command :delete do |event, *args|
	next "nothing to delete" if playlist.length == 0
	files_to_delete = []
	for arg in args
		begin
			x = Integer(arg)
			raise ArgumentError if x>playlist.length
			file = playlist[x.to_i]
			files_to_delete << file
			folder_size-=File.size("#{music_dir}"+file)
			File.delete("#{music_dir}"+file)
			event.respond("removed "+format(file))
		rescue ArgumentError, TypeError
			event.respond(x+" is not valid")
		end
	end
	playlist -= files_to_delete
	event << "deleted files"
end

bot.command :move do |event, *args|
	next "wtf are those arguments" if args.length % 2 == 1 or args.length == 0
	until args.length == 0
		songindex = Integer(args[0])
		newindex = Integer(args[1])
		if args[0] > args[1]
			listshift=1
		else
			listshift=0
		end
		song = playlist[songindex]
		if playlist.length == newindex+1
			playlist << song
		else
			playlist.insert(newindex, song)
		end
		playlist.delete_at(songindex+listshift)
		playlist_count+=listshift if playlist_count
		event.respond("moved "+format(song))
		2.times { args.shift }
	end
end
		
bot.command :playlist do |event|
	next "nothing in playlist" if playlist.length == 0
	outputs = []
	output="```"
	count = 0
	for song in playlist
		song = format(song)
		if output.length > 1800
			output+="```"
			outputs << output
			output="```"
		end
		if count == playlist_count
			output+="#{count.to_s} #{song} <= currently playing\n"
		else
			output+="#{count.to_s} #{song}\n"
		end
		count+=1
	end
	output+="```"
	outputs << output
	outputs.each do |eachoutput| event.respond(eachoutput) end
	next
end

bot.command :search do |event, arg|
	next "what am I searching" unless arg
	outputs = []
	output = "```"
	for song in playlist
		if output.length > 1800
			output+="```"
			outputs << output
			output="```"
		end
		if song.downcase.include? arg.downcase
			output+="#{playlist.find_index(song)} #{format(song)}\n"
		end
	end
	output+="```"
	outputs << output
	outputs.each do |eachoutput| event.respond(eachoutput) end
	next
end

bot.command :np do |event|
	next "nothing playing" unless playlist_count
	event.respond(format(playlist[playlist_count]))
end

bot.command :clear do |event|
	FileUtils.rm_r music_dir
	playlist = []
	folder_size = 0
	Dir.mkdir music_dir
	event << "deleted everything"
end

bot.command :shuffle do |event|
	new_playlist = playlist.shuffle
	playlist_count = new_playlist.index playlist[playlist_count] unless playlist_count == nil
	playlist = new_playlist
	event << "Shuffled btw"
end

bot.command :help do |event|
	event << "```
	Playlist commands:
		!playlist shows the playlist
		!update clears the playlist and adds all the songs from the folder
		!add (url) adds song from website see: \"rg3.github.io/youtube-dl/supportedsites.html\" for supported sites
		!addsearch (string) does a youtube search for string and the downloads first video it finds
		!addplaylist adds songs from a youtube playlist
		!delete (int) deletes a song from the playlist
		!clear deletes all songs from playlist
		!shuffle shuffles the playlist
		!move (int int) moves songs to a certain position in the playlist
		!search (string) searches for songs matching the string```"
	event << "```
	Voice commands:
		!start to play songs
		!end to disconnect from voice
		!skip {int}*optional* skips to the next song in the playlist or to given argument
		!volume (int) changes volume from 0-200 default: 100
		!pause pauses song
		!play plays song
		!loop loops the current song```"
end

def updatepl(playlist, folder_size)
	for file in Dir["#{Dir.home}/bmusic/*"]
		unless playlist.include? File.basename(file)
			folder_size+=File.size(file)
			playlist << File.basename(file)
		end
	end
	return playlist, folder_size
end

def download(link)
	download_dir = "#{Dir.home}/bmusic/"
	options = {
		no_playlist: true,
		extract_audio: true,
		audio_format: 'best',
		output: "#{download_dir}%(title)s.%(ext)s",
		max_filesize: '250m',
		audio_quality: '64k',
	}
	YoutubeDL.download link, options
end

def downloadpl(link)
	command = "youtube-dl -j --flat-playlist "+link+" |jq -r \'.id\' | sed \'s_^_https://youtube.com/v/_\'"
	links = %x[ #{command} ]
	linkslist = links.split("\n")
	for link in linkslist
		if link == linkslist[-1]
			Thread.new { download(link) }.join
		else
			Thread.new { download(link) }
		end
	end
end

def format(stringbtw)
	stringbtw = File.basename(stringbtw, ".*")
	case
	when stringbtw.include?(".webm")
		stringbtw.slice! ".webm"
	when stringbtw.include?(".m4a")
		stringbtw.slice! ".m4a"
	end
	return stringbtw
end

playlist, folder_size = updatepl([], 0)

#daemonizing
Process.daemon(false, true)
Dir.mkdir "/var/log/botmusic" unless File.directory?("/var/log/botmusic")
$stderr.reopen("/var/log/botmusic/error.log", "w")
`chmod 666 /var/log/botmusic/error.log`

bot.run
