#!/usr/bin/env ruby
require 'discordrb'
require 'youtube-dl'
require 'fileutils'
require 'faraday'
bot = Discordrb::Commands::CommandBot.new token: 'Mzk5MTUwMjY1ODkyMDc3NTY4.DgnQjg.Ghg8505YWLSjwfqbVktjQYxBdtE', prefix: '!'

class Playlist
	def initialize
		@playhash = {}
		@playlist = []
		@playcache = {}
		if File.exist? 'playhash.marshal'
			file = File.open('playhash.marshal', 'r')
			data = file.read
			file.close
			urls = []
			Marshal.load(data).values.each { |info| urls << info['url'] }
			getinfo(urls)
			@playlist = playhash.keys
		end
	end

	def getinfo(urls)
		threads = []
		output = []
		for url in urls
			threads << Thread.new(url) { |ul|
			linkclass = YoutubeDL::Video.new(ul)
			output << linkclass.fulltitle
			@playhash[linkclass.fulltitle] = {'url' => linkclass.url, 'duration' =>  linkclass.duration, 'uploader' => linkclass.uploader, 'thumbnail' => linkclass.thumbnail, 'title' => linkclass.fulltitle}
			Thread.new { checkcache(linkclass.fulltitle) }
			}
		end
		threads.each { |thr| thr.join }
		return output.join(', ')
	end

	def getplaylist(url)
		command = "youtube-dl -j --flat-playlist #{url} |jq -r \'.id\' | sed \'s_^_https://youtube.com/v/_\'"
		urls = %x[ #{command} ]
		urlslist = links.split("\n")
		getinfo(urlslist)
	end
	
	def checkcache(song)
		begin
			raise unless @playcache[song] and Faraday.head(@playcache[song]).status == 200
		rescue => e
			youclass = YoutubeDL::Video.new(@playhash[song]['url'])
			audioformats = {}
			youclass.formats.each { |x| audioformats[x[:tbr]] = x[:url] if x[:format].include? "audio only" and x[:ext] == 'webm' }
			@playcache[song] = audioformats.max[1]
		end
	end

	def move(songindex, newindex)
		song = @playlist[songindex]
		@playlist.delete_at(songindex)
		if @playlist.length == newindex
			@playlist << song
		else
			@playlist.insert(newindex, song)
		end
	end

	def shuffle(playlist_count)
		new_playlist = @playlist.shuffle
		playlist_count = new_playlist.index(@playlist[playlist_count]) unless playlist_count == nil
		@playlist = new_playlist
		return playlist_count
	end

	def delete(song)
		@playlist.delete(song)
		@playhash.delete(song)
		@playcache.delete(song)
	end

	def clear
		@playlist.clear
		@playhash.clear
		@playcache.clear
	end
	attr_reader :playlist
	attr_reader :playhash
	attr_reader :playcache
end

playClass = Playlist.new
playlist_count = nil
volume = nil
voice_bot = nil
song_loop = nil
voice_thread = Thread.new { }
playing = Thread.new { }

bot.command :start do |event|
	next "Fuck off" if playlist_count
	channel = event.user.voice_channel
	next "Get in voice" unless channel
	event.respond("playing music in "+channel.name)
	Thread.abort_on_exception = false
	bot.voice_connect(channel, encryption=true)
	voice_bot = event.voice
	voice_bot.encoder.bitrate = 64000 
	voice_bot.volume = volume if volume
	playlist_count = 0
	song_loop = nil
	voice_thread = Thread.new {
		while 1 
			begin
				sleep(0.1) until playClass.playlist[playlist_count] and playClass.playcache[playClass.playlist[playlist_count]]
				current_song = playClass.playlist[playlist_count]
				songClass.checkcache(current_song)
				link = playClass.playcache[current_song]
				playing = Thread.new { playingOut(event.channel, playClass.playhash[current_song], voice_bot) }
				voice_bot.play(IO.popen("ffmpeg -i '#{link}' -f s16le -acodec pcm_s16le -ar 48k -ac 2 -loglevel warning pipe:1"))
				playlist_count = (playlist_count+1)%playClass.playlist.length unless song_loop
			rescue => e 
				puts e
			end
		end
	}
	next
end

bot.command :skip do |event, arg|
	if arg
		playlist_count = arg.to_i 
	else
		playlist_count+=1
	end
	voice_bot.stop_playing
	playing.raise
	voice_thread.raise
	voice_bot.continue
	next
end

bot.command :end do |event|
	voice_bot.stop_playing
	voice_thread.kill
	playing.raise
 	playlist_count = nil
	voice_thread.join
	voice_bot.destroy
	event << "catch you on the flip sigh"
end

bot.command :volume do |event, arg|
	begin
		volume = Float(arg)
		next if volume>200
		volume/=100.0
		voice_bot.volume = volume
	rescue ArgumentError, TypeError
		#snarky comment
	end
	event << "Volume is #{arg}"
end

bot.command :pause do |event|
	voice_bot.pause
	event << "paused"
end

bot.command :play do |event|
	voice_bot.continue
	event << "playing"
end

bot.command :loop do |event|
	if song_loop
		song_loop = nil
		event << "stopped looping current song"
	else
		song_loop = true
		event << "looping current song"
	end
end

bot.command :add do |event, *args|
	event.channel.start_typing
	for arg in args
		unless arg.include? "https://" or arg.include? "http://"	
			args.delete(arg)
			next
		end
		if arg.include? 'index'
			args.delete(arg)
			args << arg[0..args.index('&')-1]
		end
	end
	event << playClass.getinfo(args)
end

bot.command :addplaylist do |event, arg|
	event.respond("downloading playlist")
	event.channel.start_typing
	playClass.getplaylist(arg, playhash)
	event << "finished adding playlist"
end

bot.command :delete do |event, *args|
	next "nothing to delete" if playClass.playlist.length == 0
	files_to_delete = []
	for arg in args
		begin
			x = Integer(arg)
			raise ArgumentError if x>playlist.length
			title = playClass.playlist[x.to_i]
			songs_to_delete << title
			playClass.delete(title)
		rescue ArgumentError, TypeError
			event.respond(x+" is not valid")
		end
	end
	event << "finished removing "+songs_to_delete.join(', ')
end

bot.command :move do |event, *args|
	next "wtf are those arguments" if args.length % 2 == 1 or args.length == 0
	until args.length == 0
		current_song = playClass.playlist[playlist_count] if playlist_count
		if playClass.playlist.length < args[1]
			2.times { args.shift }
			event.respond('error move place larger than playlist')
			next
		end
		playClass.move(args[0], args[1])
		playlist_count = playClass.playlist.index(current_song) if playlist_count
		event.respond("moved "+playClass.playlist[0])
		2.times { args.shift }
	end
end
		
bot.command :playlist do |event|
	event.channel.start_typing
	next "nothing in playlist" if playClass.playlist.length == 0
	outputs = []
	output="```"
	count = 0
	for song in playClass.playlist
		song
		if output.length > 1800
			output+="```"
			outputs << output
			output="```"
		end
		if count == playlist_count
			output+="#{count.to_s} #{song} <= currently playing\n"
		else
			output+="#{count.to_s} #{song}\n"
		end
		count+=1
	end
	output+="```"
	outputs << output
	event.channel.send_multiple(outputs)
	next
end

bot.command :search do |event, arg|
	next "what am I searching" unless arg
	outputs = []
	output = "```"
	for song in playClass.playlist
		if output.length > 1800
			output+="```"
			outputs << output
			output="```"
		end
		if song.downcase.include? arg.downcase
			output+="#{playClass.playlist.find_index(song)} #{song}\n"
		end
	end
	output+="```"
	outputs << output
	event.channel.send_multiple(outputs)
	next
end

bot.command :np do |event|
	next "nothing playing" unless playlist_count
	playing.raise
	playing.join
	playing = Thread.new{ playingOut(event.channel, playClass.playhash[playClass.playlist[playlist_count]], voice_bot) }
	next
end

bot.command :clear do |event|
	playClass.clear
	event << "deleted everything"
end

bot.command :shuffle do |event|
	playlist_count = playClass.shuffle(playlist_count)
	event << "Shuffled"
end

bot.command :help do |event|
	event << "```
	Playlist commands:
		!playlist shows the playlist
		!add (url) adds song from website see: \"rg3.github.io/youtube-dl/supportedsites.html\" for supported sites
		!addplaylist adds songs from a youtube playlist
		!delete (int) deletes a song from the playlist
		!clear deletes all songs from playlist
		!shuffle shuffles the playlist
		!move (int int) moves songs to a certain position in the playlist
		!search (string) searches for songs matching the string```"
	event << "```
	Voice commands:
		!start to play songs
		!end to disconnect from voice
		!skip {int}*optional* skips to the next song in the playlist or to given argument
		!volume (int) changes volume from 0-200 default: 100
		!pause pauses song
		!play plays song
		!loop loops the current song```"
end

bot.command :test do |event|
	puts 'starting'
	playClass.checkcache(playClass.playlist[0])
	puts 'done'
end

def playingOut(channel, information, voice_bot)
	begin
		channel.send_embed do |embed|
			embed.title = information['title']
			embed.description = 'uploaded by '+information['uploader']
			embed.image = Discordrb::Webhooks::EmbedImage.new(url: information['thumbnail'])
		end
		timewait = information['duration']/30.0
		message = channel.send('starting')
		position = (voice_bot.stream_time/timewait).round
		until position >= 29
			bar = []
			30.times { bar << '-' }
			bar[position] = '*'
			bar.insert(0, '<')
			bar << '>'
			message.edit('```'+bar.join+'```')
			sleep(timewait)
			position+=1
		end
	rescue => e
		puts e 
	ensure
		message.delete if message
		return
	end
end

#bot.debug = TRUE
begin
	bot.run
ensure
	file = File.open("playhash.marshal", "w")
	file.write(Marshal.dump(playClass.playhash)) if playClass.playhash
	file.close()
end
