#!/usr/bin/env ruby
require 'discordrb'
require 'youtube-dl'
require 'fileutils'
bot = Discordrb::Commands::CommandBot.new token: '$token', prefix: '!'

#playlist_count is the index of the current song that is playing
playlist_count = nil
volume = nil
voice_bot = nil
song_loop = nil
voice_thread = Thread.new { }
playing = Thread.new { }
if File.exist? 'playhash.marshal'
	file = File.open('playhash.marshal', 'r')
	data = file.read
	file.close
	playhash = Marshal.load(data)
	playlist = playhash.keys
else
	playhash = {}
	playlist = []
end

#Voice
bot.command :start do |event|
	#returns "fuck off" if already playing
	next "Fuck off" if playlist_count
	channel = event.user.voice_channel
	#returns get in voice if user not in voice
	next "Get in voice" unless channel
	event.respond("playing music in "+channel.name)
	Thread.abort_on_exception = false
	bot.voice_connect(channel, encryption=true)
	voice_bot = event.voice
	voice_bot.encoder.bitrate = 64000 
	voice_bot.volume = volume if volume
	playlist_count = 0
	song_loop = nil
	##playlist_count determines what song in the playlist is playing
	##runtime exception is raised when the song is skipped
	voice_thread = Thread.new {
		while 1 
			begin
				playlist_count = 0 if playlist_count>=playlist.length
				sleep(0.1) while playlist.length == 0
				playing = Thread.new { playingOut(event.channel, playhash[playlist[playlist_count]], voice_bot) }
				cmd = "ffmpeg -i '"+playhash[playlist[playlist_count]]['link']+"' -f s16le -acodec pcm_s16le -ar 48k -ac 2 -loglevel warning pipe:1"
				voice_bot.play(IO.popen(cmd))
				playlist_count+=1 unless song_loop
			rescue => e 
				puts e
			end
		end
	}
	next
end

bot.command :skip do |event, arg|
	playlist_count = arg.to_i if arg
	playlist_count+=1 unless arg
	voice_bot.stop_playing
	playing.raise
	voice_thread.raise
	voice_bot.continue
	next
end

bot.command :end do |event|
	voice_bot.stop_playing
	voice_thread.kill
	playing.raise
 	playlist_count = nil
	voice_thread.join
	voice_bot.destroy
	event << "goodbye"
end

bot.command :volume do |event, arg|
	begin
		volume = Float(arg)
		next if volume>200
		volume/=100.0
		voice_bot.volume = volume
	rescue ArgumentError, TypeError
		#snarky comment
	end
	event << "Volume is #{arg}"
end

bot.command :pause do |event|
	voice_bot.pause
	event << "paused"
end

bot.command :play do |event|
	voice_bot.continue
	event << "playing"
end

bot.command :loop do |event|
	if song_loop
		song_loop = nil
		event << "stopped looping current song"
	else
		song_loop = true
		event << "looping current song"
	end
end

bot.command :add do |event, *args|
	event.channel.start_typing
	for arg in args
		unless arg.include? "https://" or arg.include? "http://"	
			args-=[arg]
			next
		end
	end
	getlink(args, playhash)
	whatsthepoint = playhash.keys-playlist
	wow = "finished adding "+whatsthepoint.join(' ')
	playlist+=(playhash.keys-playlist)
	event << wow
end

bot.command :addplaylist do |event, arg|
	event.respond("downloading playlist")
	event.channel.start_typing
	getplaylist(arg, playhash)
	playlist+=(playhash.keys-playlist)
	event << "finished adding playlist"
end

bot.command :delete do |event, *args|
	next "nothing to delete" if playlist.length == 0
	files_to_delete = []
	for arg in args
		begin
			x = Integer(arg)
			raise ArgumentError if x>playlist.length
			title = playlist[x.to_i]
			songs_to_delete << title
			playhash.delete(title)
		rescue ArgumentError, TypeError
			event.respond(x+" is not valid")
		end
	end
	playlist -= songs_to_delete
	event << "finished removing "+songs_to_delete.join(' ')
end

bot.command :move do |event, *args|
	next "wtf are those arguments" if args.length % 2 == 1 or args.length == 0
	until args.length == 0
		current_song = playlist[playlist_count] if playlist_count
		songindex = Integer(args[0])
		newindex = Integer(args[1])
		song = playlist[songindex]
		playlist.delete_at(songindex)
		if playlist.length == newindex
			playlist << song
		else
			playlist.insert(newindex, song)
		end
		playlist_count = playlist.index(current_song) if playlist_count
		event.respond("moved "+song)
		2.times { args.shift }
	end
end
		
bot.command :playlist do |event|
	event.channel.start_typing
	next "nothing in playlist" if playlist.length == 0
	outputs = []
	output="```"
	count = 0
	for song in playlist
		song
		if output.length > 1800
			output+="```"
			outputs << output
			output="```"
		end
		if count == playlist_count
			output+="#{count.to_s} #{song} <= currently playing\n"
		else
			output+="#{count.to_s} #{song}\n"
		end
		count+=1
	end
	output+="```"
	outputs << output
	event.channel.send_multiple(outputs)
	next
end

bot.command :search do |event, arg|
	next "what am I searching" unless arg
	outputs = []
	output = "```"
	for song in playlist
		if output.length > 1800
			output+="```"
			outputs << output
			output="```"
		end
		if song.downcase.include? arg.downcase
			output+="#{playlist.find_index(song)} #{song}\n"
		end
	end
	output+="```"
	outputs << output
	event.channel.send_multiple(outputs)
	next
end

bot.command :np do |event|
	next "nothing playing" unless playlist_count
	playing.raise
	playing.join
	playing = Thread.new{ playingOut(event.channel, playhash[playlist[playlist_count]], voice_bot) }
	next
end

bot.command :clear do |event|
	playlist.clear
	playhash.clear
	event << "deleted everything"
end

bot.command :shuffle do |event|
	new_playlist = playlist.shuffle
	playlist_count = new_playlist.index(playlist[playlist_count]) unless playlist_count == nil
	playlist = new_playlist
	event << "Shuffled btw"
end

bot.command :help do |event|
	event << "```
	Playlist commands:
		!playlist shows the playlist
		!update clears the playlist and adds all the songs from the folder
		!add (url) adds song from website see: \"rg3.github.io/youtube-dl/supportedsites.html\" for supported sites
		!addplaylist adds songs from a youtube playlist
		!delete (int) deletes a song from the playlist
		!clear deletes all songs from playlist
		!shuffle shuffles the playlist
		!move (int int) moves songs to a certain position in the playlist
		!search (string) searches for songs matching the string```"
	event << "```
	Voice commands:
		!start to play songs
		!end to disconnect from voice
		!skip {int}*optional* skips to the next song in the playlist or to given argument
		!volume (int) changes volume from 0-200 default: 100
		!pause pauses song
		!play plays song
		!loop loops the current song```"
end

bot.command :test do |event|
	puts voice_bot.stream_time
end

def getlink(links, playhash)
	def threaded(link, playhash)
		linkclass = YoutubeDL::Video.new(link)
		cmd = "youtube-dl -f bestaudio -g "+link
		playhash[linkclass.fulltitle] = { 'link' => %x[ #{cmd} ].strip, 'url' => linkclass.url, 'duration' =>  linkclass.duration, 'uploader' => linkclass.uploader, 'thumbnail' => linkclass.thumbnail, 'title' => linkclass.fulltitle}
	end
	threads = []
	for link in links
	 	threads << Thread.new(link) { |lnk| threaded(lnk, playhash) }
	end
	threads.each { |thr| thr.join }
end

def getplaylist(link, playhash)
	command = "youtube-dl -j --flat-playlist "+link+" |jq -r \'.id\' | sed \'s_^_https://youtube.com/v/_\'"
	links = %x[ #{command} ]
	linkslist = links.split("\n")
	return getlink(linkslist, playhash)
end

def playingOut(channel, information, voice_bot)
	begin
		timewait = information['duration']/30.0
		puts timewait
		channel.send_embed do |embed|
			embed.title = information['title']
			embed.description = 'uploaded by '+information['uploader']
			embed.image = Discordrb::Webhooks::EmbedImage.new(url: information['thumbnail'])
		end
		message = channel.send('starting')
		position = (voice_bot.stream_time/timewait).round
		until position >= 29
			puts position
			bar = []
			30.times { bar << '-' }
			bar[position] = '*'
			bar.insert(0, '<')
			bar << '>'
			message.edit('```'+bar.join+'```')
			sleep(timewait)
			position+=1
		end
	rescue => e
		puts e 
	ensure
		message.delete if message
		return
	end
end

#bot.debug = TRUE
begin
	bot.run
ensure
	file = File.open("playhash.marshal", "w")
	file.write(Marshal.dump(playhash))
	file.close()
end
